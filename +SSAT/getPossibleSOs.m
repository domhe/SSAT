function slowOsci = getPossibleSOs(data, Fs, stages, lengthCriteria)




%run detection of all possible slow waves


% get Idx of 0-Xings
zeroXings        = diff(sign(data));  % -2 = pos2negZeroXing; 2 = neg2posZeroXing
pos2negZeroXings = find(zeroXings<0);
neg2posZeroXings = find(zeroXings>0);

% make sure sequence starts with a pos2negZeroXings
if neg2posZeroXings(1) < pos2negZeroXings(1)
    neg2posZeroXings(1) = [];
end

% make sure sequence end with a pos2negZeroXings (else no full SO at the end)
if neg2posZeroXings(end) > pos2negZeroXings(end)
    neg2posZeroXings(end) = [];
end


% save start, mid and endpoint of each slow oscillation
slowOsci = table(pos2negZeroXings(1:end-1)', neg2posZeroXings', pos2negZeroXings(2:end)',...
    'VariableNames', {'startPoint', 'midPoint', 'endPoint'});

% get length of negativ and positiv half wave
slowOsci.negLengths   = slowOsci.midPoint - slowOsci.startPoint;
slowOsci.posLengths   = slowOsci.endPoint - slowOsci.midPoint;
slowOsci.wholeLengths = slowOsci.negLengths + slowOsci.posLengths;


% add sleep stage to slowOsci table
slowOsci.sleepStage = stages(slowOsci.midPoint); %sleep stage is taken at the midPoint of the SO


% delete slow waves during wake, n1 and rem
idx = slowOsci.sleepStage ~= 2:4;
idx = sum(idx, 2) == 3;
slowOsci(idx, :) = [];

% delete slow waves that do not fit the lengthCriterion
if ~isempty(lengthCriteria.neg)
    idx = slowOsci.negLengths < lengthCriteria.neg(1) * Fs   | ...
          slowOsci.negLengths > lengthCriteria.neg(2) * Fs;
    slowOsci(idx, :) = [];
end
if ~isempty(lengthCriteria.pos)
    idx = slowOsci.posLengths < lengthCriteria.pos(1) * Fs   | ...
          slowOsci.posLengths > lengthCriteria.pos(2) * Fs;
    slowOsci(idx, :) = [];
end
if ~isempty(lengthCriteria.whole)
    idx = slowOsci.wholeLengths < lengthCriteria.whole(1) * Fs   | ...
          slowOsci.wholeLengths > lengthCriteria.whole(2) * Fs;
    slowOsci(idx, :) = [];
end



% from here we run through all possibel SO-events and calculate their
% amplitudes, etc.
for event = 1:height(slowOsci)
    % get event
    soStart = slowOsci.startPoint(event);
    soEnd   = slowOsci.endPoint(event);
    tmpData = data(soStart:soEnd+1);
    
    % find and save posPeaks
    pks = findpeaks(tmpData);
    pks = max(pks);
    loc = soStart + find(tmpData == pks) -1;
    slowOsci.posPeakValue(event) = pks;
    slowOsci.posPeakLoc(event)   = loc;
    
    % find and save negPeaks
    pks = findpeaks(-tmpData);
    pks = max(pks);
    loc = soStart + find(tmpData == -pks) -1;
    slowOsci.negPeakValue(event) = -pks;
    slowOsci.negPeakLoc(event)   = loc;
    
    %save peak2peak value
    slowOsci.Peak2PeakValue(event) = -slowOsci.negPeakValue(event)...
        + slowOsci.posPeakValue(event);
    
end
