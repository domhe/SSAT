classdef SlowOscillations < SSAT.EventDetectionInterface
    %UNTITLED11 Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function sObj = SlowOscillations(PSG)
            sObj.psg    = PSG; %copy reference to PSG object into SlowOscillation object
            sObj.stages = PSG.sleepStages.upsampled;
            sObj.type   = 'SlowOscillations';
        end
        % cfg.method = 'Massimini, Riedner, Mölle,
        % mfSoDetectionFunctionName
        function detect(sObj, cfg)
            %set defaults
            cfg.method   = ft_getopt(cfg, 'method', 'Massimini');
            cfg.channel  = ft_getopt(cfg, 'channel', 'all');
            switch cfg.method
                case 'Massimini'
                    sObj.criteria = 'Massimini';
                    sObj.sleepEvents = SSAT.Massimini(sObj, cfg);
                case 'Moelle'
                    %sObj.sleepEvents = SSAT.Moelle(sObj, cfg);
            end
            
        end
        function createBVmarkerfile(sObj)
            nElectrodes = numel(sObj.psg.data.label);
            markerDescription = {'SO_start' 'SO_negPeak' 'SO_mid' 'SO_posPeak' 'SO_end'};
            
            
            for elec = 1:nElectrodes
                elecName  = sObj.psg.data.label{elec};
                fs        = sObj.psg.data.fsample; 
                stages    = repmat(sObj.sleepEvents.(elecName).sleepStage, 5, 1);
                markerDescription = repmat(markerDescription', size(sObj.sleepEvents.(elecName), 1), 1);
                markerDescription = strcat(markerDescription, '_S_', num2str(stages));
                
                data = [sObj.sleepEvents.(elecName).startPoint...
                    sObj.sleepEvents.(elecName).negPeakLoc...
                    sObj.sleepEvents.(elecName).midPoint...
                    sObj.sleepEvents.(elecName).posPeakLoc...
                    sObj.sleepEvents.(elecName).endPoint];
                
                data = reshape(data', [], 1);
                name = sObj.psg.name;
                markerDescription(:,2) = num2cell(data);
                markerDescription = markerDescription';
                mName      = [name '_' sObj.type '_' elecName '.vmrk'];
                fid        = fopen(mName, 'w');
                SR         = strcat('Sampling rate:',num2str(fs),'Hz');
                SI         = strcat('SamplingInterval:',num2str(1000/fs),'ms');
                FIDI       = strcat(SR,',', SI,'\r\n Type, Description, Position, Length, Channel\r\n');
                fprintf(fid, FIDI);
                fprintf(fid,'SO_Marker, %s, %d, 1, all\r\n', markerDescription{:});
                fclose(fid);
            end
            
        end
    end
end

