
classdef EventDetectionInterface <  handle
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = protected)
        psg;
        stages;
        sleepEvents;
        type;
        criteria;
    end
    methods (Abstract)
        obj = detect(obj);
    end
    methods
        function save(obj, varargin)
            name = obj.psg.name;
            cfg = finputcheck(varargin, ...
                { 'path'    'string'   []  pwd; ...
                'format'  'string'   { '.mat' '.json' '.txt'}  'mat'});
            if ischar(cfg)
                error(cfg);
            end
            name = fullfile(cfg.path, [name  '_' obj.type]);
            if strcmp(cfg.format, '.mat') %default
                toSave = obj.sleepEvents;
                save(name, 'toSave');
            else
                jsonStr = jsonencode(obj.sleepEvents);
                fid = fopen(name, 'w');
                if fid == -1, error('Cannot create JSON file'); end
                fwrite(fid, jsonStr, 'char');
                fclose(fid);
            end
        end
    end
end


