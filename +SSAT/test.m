clear;
clear classes;
import SSAT.*;


%init a PSG object
%see doc PSG for more information
cfg = [];
cfg.dataset     = 'dummyPSG.vhdr';
cfg.sleepStages = 'dummyPSG.txt';
cfg.hpfileter   = 'yes';
cfg.hpfreq      = 0.1;
%cfg.resamplefs  = 64;
psg = PSG(cfg);

%plot a hypnogram using a PlotSizer object
%see doc PlotSizer for more information about the class
myHypnogram = PlotSizer(psg.getHypnogram()); %init
myHypnogram.resize(16, 8) %resize(x, y) in cm
%myHypnogram.plot()
myHypnogram.save('format', '-djpeg')


%get objective sleep data
objSpeeData = psg.getObjectiveSleepData();


%detect so
cfg        = [];
cfg.method = 'Massimini';
psg.slowOscis.detect(cfg);
psg.slowOscis.save('format', '.json');
psg.slowOscis.createBVmarkerfile();

%calculate sleep spectogram
psg.calculateSleepSpectogram([30 5], [15 29]);
specto = psg.getSleepSpectogram(); 


