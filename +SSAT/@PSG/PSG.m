classdef PSG < handle
    
    
    %PSG holds a EEG/PSG dataSet + associated sleep satging and provides methods to
    %-create a figure handle for a standard Hypnogram
    %-calculate + create a figure handle for a (full-night) spectogramm following Prerau et al., 2017
    %-calculate standard objective sleep data (automatically initiated when a new PSG instance is created)
    %
    properties (SetAccess = protected)
        data                %dataSet
        name
        sleepStages         %sleepStages.normal (1 each 30s [0 1 1 2 ])
        objectiveSleepData; %stores SOL, etc.
        sleepSpectogram     %spect (spectral data), .time (time information), . freq (frequency information)
        slowOscis;
        spindles;
    end
    
    methods
        
        function obj = PSG(cfg)
            % PSG constructor needs a struct with fields 'sleepStages.normal' and
            % 'dataset'. Additionaly, the structure can contain fields
            % accepted in ft_preprcessing and cfg.resamplefs of ft_resampledata.
            % e.g. cfg = [];
            % cfg.dataSet = '~/myData';
            % cfg.sleepStages.normal = '~/myStages'; % needs to be stores in a
            % .txt file.
            % The constructor also inits the calculation of standard
            % objective sleep metrics, such as SOL etc. 
            if ~all(isfield(cfg, {'sleepStages', 'dataset'}))
                error(['PSG constructor needs a struct with fields',...
                    ' "sleepStages.normal" and "dataset". Additionally, ',...
                    'ft_preprocessing parameters, can be added to the struct, too']);
            end
            obj.name = cfg.dataset;
            % do user defined preprocesssing
            if isfield(cfg, 'resamplefs')
                rs.resamplefs = cfg.resamplefs;
                cfg = rmfield(cfg, 'resamplefs');
                obj.data = ft_preprocessing(cfg);
                obj.data = ft_resampledata(rs, obj.data);
            else 
                obj.data = ft_preprocessing(cfg);
            end
            obj.sleepStages.normal    = dlmread(cfg.sleepStages); %read sleepStages
            obj                       = resampleSleepStages(obj); %also save sleepStages in us manner 
            obj.objectiveSleepData    = calculateObjectiveSleepData(obj);
            obj = initEvents(obj);
            obj.data.label = matlab.lang.makeValidName(obj.data.label); % make sure channel names are valid
            [~, obj.name] = fileparts(obj.name);
        end
        
        % get figure handle of a hynogram -> can be plotted using a PlotSizer object
        fig = getHypnogram(obj);
        
        % SleepSpectpgram
        % calculate sleepSpectogram (for details see the implementation of the function
        obj = calculateSleepSpectogram(mvWindow, tapers, varargin);
        
        % getter function to receive the sleepSpectpgram
        sleepSpectogram = getSleepSpectogram(obj) %evtl eigene klasse mit bandextraction
        

        % getter function to receive standard objective sleep metrics, such as SOL etc.
        objectiveSleepData = getObjectiveSleepData(obj);
        
        % ResampleStages
        obj = resampleSleepStages(obj, varargin);
        
        
        %think over this one and make PRIVATE
        function obj = initEvents(obj)
            obj.slowOscis = SSAT.SlowOscillations(obj);
            obj.spindles  = SSAT.SlowOscillations(obj);
        end
        
        
        % heart rate, respiration, rems, spindles, slow wavees
    end
    
end

