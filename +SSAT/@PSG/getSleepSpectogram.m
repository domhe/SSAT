function sleepSpectogram = getSleepSpectogram(obj)
% getter function to receive the sleepSpectpgram
if ~isfield(obj.sleepSpectogram, 'spect')
    warning('you need to first calculate the spectogram using calculateSleepSpectogram')
end
    sleepSpectogram = obj.sleepSpectogram;
end