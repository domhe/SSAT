function obj = resampleSleepStages(obj, varargin)
% getResampledSleepStages: upsamples 30s sleep markers to a userdefined distance-rate
%
% Usage:
%        >> stages = getResampledSleepStages(PSG, 'key1',value1, 'key2',value2, ... );
% Required inputs:
%       PSG object with fields:
%       data            = vector of 30s sleep stages ([0 0 0 1 1 2 2 3 3 3 5...])
%       data.fsample    = data sampling rate (Hz)
%       data.sampleinfo = length (in sampling points) of the corresponding eeg
%
% Optional parameters:
%       markerdistance = distance of upsampled SS in seconds;
%                        - markerdistance needs to fullfill mod(30, markerdistance) == 0
%                        - markerdistance needs to be < 30 otherwise it would ne no up- but downsampling of SS
%                        - default = each samplingPoint
%       adjustLength   = adjust stagings to eegLength ['last', 'undefined','no']
%                        prolong stages up to the length of the eegFile (in case it is shorter).
%                        - 'last' fill up with staging of last staged epoch
%                        - 'undefined' (default) fill up with -1 = undefined ss
%                        - 'no': do not fill up
%
% Output:
%       - stages: upsampled stages [stage samplinpoint]
%
% Requirements / Dependencies:
% finputecheck.m of eeglab toolbox (https://sccn.ucsd.edu/eeglab/index.php)
%
% Further Information:
% Note that each marker gives information about which sleep stage was
% present from marker to marker-markerdistance. Hence, one needs to keep in
% mind to segment from marker-x:marker; not from marker:marker+x
%
%
% Copyright (C) Dominik Heib, Centre for Cognitive Neuroscience Salzburg, dominik.heib@sbg.ac.at
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.



% basic input check of required nargin
if nargin < 1
    help ssat_upsampleSS; return
end


% get necessary info from PSG object (all very smalll, hence can be copied
% into new vars)
stages    = obj.sleepStages.normal;
eegLength = length(obj.data.trial{1,1});
Fs        = obj.data.fsample;

% check optional args + set defaults
try opts = struct(varargin{:});
catch
    error('Argument error in the {''param'', value} sequence');
end
if isfield(opts, 'markerdistance')
    if ~mod(30, opts.markerdistance)==0
        error('mod(30, markerdistance) needs to be 0');
    end
end
opts = finputcheck(varargin, ...
    { 'markerdistance'  'integer'  []             1/Fs; ...
    'adjustLength'    'string'   {'last', 'undefined', 'no'}  'undefined'});
if ischar(opts), error(opts); end
markerDistance = opts.markerdistance;
adjustLength   = opts.adjustLength;

n = numel(stages);

%upsample 30s stageInformation
stageSP = zeros(Fs*30, n);
for n = 1:n
    stageSP(:,n) = repmat(stages(n), Fs*30, 1);
end
stageSP      = reshape(stageSP,[],1); % for each SP one stageMarker
stLen        = length(stageSP');
stageSP(:,2) = 1:stLen;     % sampling point

%if more stages than eeg is long, delete stages at the end
if stLen > eegLength
    stageSP = stageSP(1:eegLength, :);
    stLen   = length(stageSP');
end

%if adjustLength option, either fill with zeros, take last stage and
%replicate or write -1 (undefined)
if ~strcmp(adjustLength, 'no')
    if eegLength > stLen
        if strcmp(adjustLength, 'last')
            stageSP(stLen+1:eegLength, 1) = stageSP(stLen, 1);
        elseif strcmp(adjustLength, 'undefined')
            stageSP(stLen+1:eegLength, 1) = -1;
        end
        stageSP(stLen+1:eegLength, 2) = stageSP(stLen, 2)+1:eegLength;
        stLen = length(stageSP);
    end
end

%idxs of sleep stage at markerDistance-positions
idx = Fs*markerDistance:Fs*markerDistance:stLen;
obj.sleepStages.upsampled  = stageSP(idx, :);
end

