function objectiveSleepData = getObjectiveSleepData(obj)
% getter function to receive the objectiveSleepData
    objectiveSleepData = obj.objectiveSleepData;
end