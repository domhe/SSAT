% getHypnogram: creates a figure handle to a standard 0815 hypnogramm
%
% Usage:
%        >> getHypnogram(obj);
% Required inputs: 
%       PSG object including a field obj.sleepStages [0 0 0 1 1 2 2 3 3 3 5...]
%
%
% Requirements / Dependencies:
% none under Matlab 2016a
%
% Copyright (C) Dominik Heib, Centre for Cognitive Neuroscience Salzburg, dominik.heib@sbg.ac.at
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

function figure1 = getHypnogram(obj)
% getHypnogram: creates a figure handle to a standard 0815 hypnogramm
% Usage: getHypnogram()

stagings = obj.sleepStages.normal;

% recode recStagings
recStagings = stagings;
recStagings(recStagings==1) = -2;
recStagings(recStagings==2) = -3;
recStagings(recStagings==3) = -4;
recStagings(recStagings==5) = -1;

time = 1:numel(stagings);

% plot Hypnogramm
figure1 = figure;
set(figure1, 'Visible','off')
figure1.Name = 'Hypnogram';
axes1 = axes('Parent',figure1);
hold(axes1,'on');
stairs(time,recStagings,'LineWidth',2, 'color', 'k');
hold on

% highlight R periods
recStagings(end) = -10;  % mark end
idx  = find(diff(recStagings)) + 1;
idx1 = find(recStagings(idx) == -1);
for i = 1:numel(idx1)
    start = idx(idx1(i));
    stop  = idx(idx1(i) + 1);
    rectangle('Position', [start -1.3 stop-start 0.6], 'FaceColor', 'k', 'EdgeColor','k', 'LineWidth', 2)
end

% add labels and stuff
ylim(axes1,[-6 2]);
box(axes1,'on');
set(axes1,'YTick',[-4 -3 -2 -1 0],'YTickLabel',{'N3','N2','N1','R','W'});
ylabel('Sleep Stage')
xlim(axes1,[0 numel(stagings)]);
set(axes1,'XTick',0:120:numel(stagings), 'XTickLabel', 0:numel(stagings)/120);
xlabel('hours')

end


