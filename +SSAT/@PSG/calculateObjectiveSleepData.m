% get objecitve sleep data from sleepStages.normal
% needs to be extended with other measures, such as TST, sleep
% efficiency etc.
% SOL:
% Per default, sleep onset is defined as 5 minutes of continous NREM sleep
% Optional Arguments as key-value pairs:
% minContinousSleepMinutes (defines min. amount of cont. sleep in minutes)
% e.g. 'minContinousSleepMinutes', 10
% solCrit = NREM or N2 or N3
% e.g. 'solCrit', 'NREM' when a subjects spends minContinousSleepMinutes
% continously in N1, N2 or N3
% e.g. 'solCrit', 'N2' when a subjects spends minContinousSleepMinutes
% continously in N2 or N3


function objectiveSleepData = calculateObjectiveSleepData(obj, varargin)
% check optional args + set defaults
try opts = struct(varargin{:});
catch
    error('Argument error in the {''param'', value} sequence');
end
opts = finputcheck(varargin, ...
    { 'minContinousSleepMinutes' 'integer' []   5; ...
      'solCrit'                  'string'   {'NREM', 'N2', 'N3'}  'NREM'});
if ischar(opts), error(opts); end

% change minContinousSleepMinutes input to epochs
minContinousSleepMinutes = opts.minContinousSleepMinutes * 2;

% check solCrit
if strcmp(opts.solCrit, 'NREM')
    solCrit = 1:4;
elseif strcmp(opts.solCrit, 'N2')
    solCrit = 2:4;
elseif strcmp(opts.solCrit, 'N3')
    solCrit = 3:4;
end


objectiveSleepData.N0_min   = sum(obj.sleepStages.normal == 0) / 2;
objectiveSleepData.N1_min   = sum(obj.sleepStages.normal == 1) / 2;
objectiveSleepData.N2_min   = sum(obj.sleepStages.normal == 2) / 2;
objectiveSleepData.N3_min   = sum(obj.sleepStages.normal == 3 | obj.sleepStages.normal == 4) / 2;
objectiveSleepData.NREM_min = sum(ismember(obj.sleepStages.normal, 1:4)) / 2;
objectiveSleepData.R_min    = sum(obj.sleepStages.normal == 5) / 2;
objectiveSleepData.TIB      = numel(obj.sleepStages.normal) / 2;
objectiveSleepData.SOL      = getSOL(obj, minContinousSleepMinutes, solCrit);

end



function SOL = getSOL(obj, minContinousSleepMinutes, solCrit)

SOL = find(ismember(obj.sleepStages.normal, solCrit) == 1);
breakout = 1;
while breakout < numel(obj.sleepStages.normal) - minContinousSleepMinutes
    criterion = 0;
    idx = SOL(breakout);
    for s = 1:minContinousSleepMinutes
        if ~obj.sleepStages.normal(idx+s)
            break;
        end
        criterion = criterion + 1;
    end
    if criterion == minContinousSleepMinutes
        break
    end
    breakout = breakout + 1;
end
SOL = (idx-1) / 2;
end