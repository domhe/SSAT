function obj = calculateSleepSpectogram(obj, mvWindow, tapers, varargin)
% getSleepSpec: uses chronux toolbox functions to get the spectrogramm of a an input signal using
% multitapers (dpss). 
%
% Usage: calculateSleepSpectogram(mvWindow, tapers, varargin);
% Required inputs: 
%       mvWindow    = [window winstep] i.e length of moving window and stepsize (in s)
%       tapers      = [TW K] numeric vector where TW is the time-bandwidth product and K is the 
%                     number of tapers to be used (less than or equal to 2TW-1). 
% 
% Optional parameters: 
%       pad        = Padding factor for the FFT. Values between -1:2 are allowed. 
%                    -1 corresponds to no padding, 0 corresponds to padding
%                    to the next highest power of 2 etc. Default = 0.
%       fpass      = [lower upper] i.e. frequencies of interest (in Hz). Values between [0 Fs/2] are allowed. Default = [0 Fs/4]. 
%
% Output: output is written in PSG.speepSpectogram: can be accessed by
% getSpeepSpectogram.
%       - spect: spectogram scaled in dB
%       - time : time points in s
%       - freq : freqresolution
%
% Recommendations of settings for sleep data are based on: Prerau et al., 2017
% Full Night Data (8hrs)               : mvWindow = [30 5]; tapers = [15 29];
% transient dynamics (spindles, KCs)   : mvWindow = [2.5 0.05]; tapers = [5 9];
%
% Requirements / Dependencies:
% chronux toolbox: http://chronux.org/
% finputecheck.m of eeglab toolbox (https://sccn.ucsd.edu/eeglab/index.php)
% Matlab'S signal toolbox
%
% Copyright (C) Dominik Heib, Centre for Cognitive Neuroscience Salzburg, dominik.heib@sbg.ac.at
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

data = obj.data.trial{1}';
Fs   = obj.data.fsample;

% basic input check of required nargin
%-------------------
if nargin < 3
    help getSleepSpec; return
elseif size(data, 1) < size(data, 2)
    error('data has more channels than samples!')
elseif sum(size(mvWindow) ~= [1 2])
    error('check dimension [1 2] of mvWindow');
elseif sum(size(tapers) ~= [1 2])
    error('check dimension [1 2] of taper');
end

% basic input check optional args + set defaults
try opts = struct(varargin{:});
catch
    error('Argument error in the {''param'', value} sequence');
end
opts = finputcheck(varargin, ...
               { 'fpass'       'real'   [0 Fs/2]  [0 Fs/4]; ...
                 'pad'        'real'   [-1 2]       0});
if ischar(opts), error(opts); end
opts.tapers = tapers;
opts.Fs     = Fs;


% calculate spectogramm 
[spect, time , freq]  = mtspecgramc(data, mvWindow, opts);
spect = 10*log10(spect);
time  = time - 1/Fs;

obj.sleepSpectogram.spect = spect;
obj.sleepSpectogram.time  = time;
obj.sleepSpectogram.freq  = freq;
end

