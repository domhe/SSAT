function events = Massimini(sObj, cfg)
fprintf('Slow Oscillation detection (Massimini) started...\n');
%-------------------------
% PREPROCESSING
%-------------------------
% high-pass filter (0.1 Hz), low-pass filter (4 Hz); no filter details provided
cfg = [];
cfg.plotfiltresp = 'yes'
cfg.hpfilter = 'yes';
cfg.hpfreq   = 0.1;
cfg.lpfilter = 'yes';
cfg.lpfreq   = 4;



%apply filter + data reduction (channels)
data  = ft_preprocessing(cfg, sObj.psg.data);
figure
plot(data.trial{1,1}(1,:)) hold on
plot(sObj.psg.data.trial{1,1}(1,:))

%-------------------------
% SET Detection CRITERIA [min, max]
%-------------------------
lengthCriteria.neg   = [0.3 1];
lengthCriteria.pos   = [];
lengthCriteria.whole = [];
peakCriteria.neg     = -80;
peakCriteria.pos     = [];
peakCriteria.p2p     = 140;

%-------------------------
%loop start
%-------------------------
nElectrodes = size(data.trial{1, 1}, 1);
clc
ft_progress('init', 'etf', 'Detecting Slow Oscillations...');

for electrode = 1:nElectrodes
    elecName = data.label{electrode}; 
    ft_progress(electrode/nElectrodes, 'Detecting Slow Oscillations on electrode %s ', elecName);

    
    %get possible slow oscillations
    slowOsci = SSAT.getPossibleSOs(data.trial{1,1}(electrode,:), data.fsample, sObj.stages, lengthCriteria);
    
    %apply peak criteria
    idx = slowOsci.negPeakValue   > peakCriteria.neg |...
        slowOsci.Peak2PeakValue < peakCriteria.p2p;
    slowOsci(idx, :) = [];
    events.(elecName) = slowOsci;
end

 ft_progress('close')

end

% 1 = Riedner et al. 2007:
% neg. Length    : 0.25-1s
% Peak criteria  : no
% recommended PP : Lp30 (AA), Bp0.5-4Hz (chebyII), DS 100Hz

%
% 3 = Mölle et al. 2009:
% whole Length   : 0.9-2s
% Peak criteria  :
% - negPeak smaller than 2/3 of the average of all NREM (without N1) negPeaks
% - P2P > than 2/3 of the average of all NREM (without N1) P2P's
% recommended PP : Lp30 (AA), , Hp0.01, Lp2Hz, DS 100Hz
%
%
