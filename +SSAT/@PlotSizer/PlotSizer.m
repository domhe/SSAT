classdef PlotSizer
    % Plotsizer holds functions to resize and save a matlab plot to disc
    
    properties (SetAccess = private)
        figure
    end
    
    methods
        function obj = PlotSizer(fig)
            % constructor: takes a figure handle as input: PlotSizer(fig)
            obj.figure = fig;
        end
        
        % resize the figure: resize(x, y), with x and y in cm
        resize(obj, x, y)
        
        % plot the figure: plot() or plot(x, y) with x and y resizing the figure to
        %x and y cm.
        plot(obj, x, y)
        
        % save the figure: save()
        % optional arguments are 
        % 'path': where to save the figure: 'path', '~/myPictures'
        % 'format': 
        % possible formats are '-dpng' '-tiff' '-djpeg' '-dpng' '-dmeta' '-dtiffn' '.fig'
        % e.g.'format', '-dpng' (defaulr = '.fig')
        save(obj, varargin)
        
        
        
    end
end

