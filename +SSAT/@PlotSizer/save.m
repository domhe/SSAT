function save(obj, varargin)
% save the figure: save()
% optional arguments are
% 'path': where to save the figure: 'path', '~/myPictures'
% 'format':
% possible formats are '-dpng' '-tiff' '-djpeg' '-dpng' '-dmeta' '-dtiffn' '.fig'
% e.g.'format', '-dpng' (defaulr = '.fig')

cfg = finputcheck(varargin, ...
    { 'path'    'string'   []  pwd; ...
    'format'  'string'   { '-dpng' '-djpeg' '-dtiffn' '.fig'}  '.fig'});
if ischar(cfg)
    error(cfg);
end
if strcmp(cfg.format, '.fig') %default
    savefig(obj.figure, fullfile(cfg.path, obj.figure.Name));
else
    print(obj.figure, fullfile(cfg.path, obj.figure.Name), cfg.format)
end
end