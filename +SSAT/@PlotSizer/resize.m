function resize(obj, x, y)
%resize the figure: resize(x, y), with x and y in cm
            scrsz = get(0,'ScreenSize');
            set(obj.figure, 'PaperUnits', 'centimeters');
            set(obj.figure, 'Position', [scrsz(4)/4 scrsz(3)/4 x*50 y*50]);
            set(obj.figure, 'PaperPositionMode', 'auto');
end