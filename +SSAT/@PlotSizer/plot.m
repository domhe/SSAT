function plot(obj, x, y)
 % plot the figure: plot() or plot(x, y) with x and y resizing the figure to
 % x and y cm.
if nargin == 3
    obj.resize(x, y)
end
set(obj.figure, 'Visible','on');
end